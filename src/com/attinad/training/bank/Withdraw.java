package com.attinad.training.bank;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

public class Withdraw implements Transaction{
	public void trans(int csid) {
		String type1=
				"";
		String type = "withdrawal";
		int acbal=0;
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bank", "root", "root");
			System.out.println("---CASH WITHDRAWAL--");
			int cid = csid;
			System.out.println("Enter Your account number");
			Scanner sc = new Scanner(System.in);
			int acno = sc.nextInt();
			PreparedStatement st = con.prepareStatement("select * from account_details where cid=? and accnum=?");
			st.setInt(1, cid);
			st.setInt(2, acno);
			ResultSet rs = st.executeQuery();
			while (!rs.next()) {
				System.out.println("Invalid account number!!!!");
				System.exit(0);
			}

			System.out.println("enter the payment mode for initail deposit..press 1 for CASH..2 for CHEQUE");
			System.out.println("press 3 for fund transfer");
			int ch = sc.nextInt();
			if (ch == 1)
				type1 = "Cash";
			else if(ch==2)
				type1 = "cheque";
						
			System.out.println("enter the amount for WITHDRAWAL");

			int amt = sc.nextInt();
			st=con.prepareStatement("select accbal from account_details where cid=?");
			st.setInt(1, cid);
			ResultSet rs1=st.executeQuery();
			while(rs1.next())
				acbal=rs.getInt(4);
			if((acbal-amt)<1000)
			{
				System.out.println("Limit exceeded..transaction cannot be processed");
			System.exit(0);	
			}
				
			st = con.prepareStatement(
					"insert into transaction(type,accnum,cid,amt,mode,transdate) values (?,?,?,?,?,NOW())");
			st.setString(1, type);
			st.setInt(2, acno);
			st.setInt(3, cid);
			st.setInt(4, amt);
			st.setString(5, type1);
			int result = st.executeUpdate();
			System.out.println("Transaction successfully completed :)");
			st = con.prepareStatement("update account_details set accbal=accbal-?");
			st.setInt(1, amt);
			int result1=st.executeUpdate();
			System.out.println("Account balance updated");

		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
