package com.attinad.training.bank;

import java.util.Scanner;

import java.sql.*;

public class View {
	void viewdetails() {

		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bank", "root", "root");
			System.out.println("login to view details");
			Login l = new Login();
			int cid = l.userlogin();
			Scanner sc=new Scanner(System.in);
			System.out.println("enter name,customer id or account number to search details");
			System.out.println("press 1 to search by customer id or account number and press 2 to seach by name");
			int i=sc.nextInt();
			if(i==1)
			{
			int input=sc.nextInt();
			System.out.println("enter account number or customer id");
			PreparedStatement st=con.prepareStatement("select customer.name,customer.pan,customer.cid,account_details.accnum,account_details.accbal,transaction.type,transaction.amt,"
					+ "transaction.transdate,transaction.mode from customer inner join account_details on account_details.cid=customer.cid inner join transaction on transaction.accnum=account_details.accnum where customer.cid=? or account_details.accnum=? " );
       st.setInt(1, input);
       st.setInt(2, input);
       ResultSet rs=st.executeQuery();
       while(rs.next())
       {
    	   System.out.println("customer name: "+rs.getString(1));
    	   System.out.println("pan no: "+rs.getInt(2));
    	   System.out.println("customer id: "+rs.getInt(3));
    	   System.out.println("account number:"+rs.getInt(4));
    	   System.out.println("account balance: "+rs.getInt(5));
    	   System.out.println("Transaction type: "+rs.getString(6));
    	   System.out.println("Transaction amount: "+rs.getInt(7));
    	   System.out.println("Transaction mode:"+rs.getString(8));
    	   System.out.println("_____________________________________");
       }
			}
			else if(i==2){
				System.out.println("enter name as in pan");
			String s=sc.next();
		
			PreparedStatement st=con.prepareStatement("select customer.name,customer.pan,customer.cid,account_details.accnum,account_details.accbal,transaction.type,transaction.amt,"
					+ "transaction.transdate,transaction.mode from customer inner join account_details on account_details.cid=customer.cid inner join transaction on transaction.accnum=account_details.accnum where customer.name=?" );
       st.setString(1, s);
       ResultSet rs=st.executeQuery();
       while(rs.next())
       {
    	   System.out.println("customer name: "+rs.getString(1));
    	   System.out.println("pan no: "+rs.getInt(2));
    	   System.out.println("customer id: "+rs.getInt(3));
    	   System.out.println("account number:"+rs.getInt(4));
    	   System.out.println("account balance: "+rs.getInt(5));
    	   System.out.println("Transaction type:"+rs.getString(6));
    	   System.out.println("Transaction amount: "+rs.getInt(7));
    	   System.out.println("Transaction mode: "+rs.getString(8));
    	   System.out.println("_____________________________________");
       }
			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
