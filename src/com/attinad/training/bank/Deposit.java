package com.attinad.training.bank;

import java.sql.*;
import java.sql.DriverManager;
import java.util.Scanner;

public class Deposit implements Transaction {

	@Override
	public void trans(int csid) {
		String type1;
		String type = "Deposit";
		// TODO Auto-generated method stub
		try {
			Class.forName("com.mysql.jdbc.Driver");
			Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bank", "root", "root");
			System.out.println("---CASH DEPOSIT--");
			int cid = csid;
			System.out.println("Enter Your account number");
			Scanner sc = new Scanner(System.in);
			int acno = sc.nextInt();
			PreparedStatement st = con.prepareStatement("select * from account_details where cid=? and accnum=?");
			st.setInt(1, cid);
			st.setInt(2, acno);
			ResultSet rs = st.executeQuery();
			while (!rs.next()) {
				System.out.println("Invalid account number!!!!");
				System.exit(0);
			}

			System.out.println("enter the payment mode for initail deposit..press 1 for CASH..2 for CHEQUE");
			int ch = sc.nextInt();
			if (ch == 1)
				type1 = "Cash";
			else
				type1 = "cheque";
			System.out.println("enter the amount for deposit");

			int amt = sc.nextInt();
			st = con.prepareStatement(
					"insert into transaction(type,accnum,cid,amt,mode,transdate) values (?,?,?,?,?,NOW())");
			st.setString(1, type);
			st.setInt(2, acno);
			st.setInt(3, cid);
			st.setInt(4, amt);
			st.setString(5, type1);
			int result = st.executeUpdate();
			System.out.println("Transaction successfully completed :)");
			st = con.prepareStatement("update account_details set accbal=accbal+?");
			st.setInt(1, amt);
			int result1=st.executeUpdate();
			System.out.println("Account blance updated");

		} catch (Exception e) {
			System.out.println(e);
		}

	}
	

		
	
}
